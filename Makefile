# For production
up:
	@docker-compose up -d --remove-orphans
pull:
	@docker-compose pull
update:
	@docker-compose pull && docker-compose up -d --remove-orphans
down:
	@docker-compose down
clean:
	@docker system prune
# For development
build:
	@docker-compose build 
dev:
	@docker-compose -f docker-compose.dev.yml up --remove-orphans
