const { join }      = require( "path" );

const cookieParser  = require( "cookie-parser" );
const decodeJWT     = require( join( __dirname, "shared", "decodeJwt" ));
const package       = require( join( __dirname, "package.json" ));

const backEndUrl    = "http://blogger-api:9000"

module.exports = {
    mode: "universal",
    server: {
        host: "0.0.0.0",
        port: 3000
    },
    env: {
        VERSION: package.version,
        HOST: "blogger.kotir.space",
    },
    /*
    ** Headers of the page
    */
    head: {
        title: "Blogger",
        htmlAttrs: {
            lang: "en",
            dir: "ltr"
        },
        noscript: [{ innerHTML: "This website requires javascript to run. Please enable it in browser settings." }],
        meta: [{
            charset: "utf-8"
            },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1",
                hid: "viewport"
            },
            {
                name: "description",
                content: "Place to blog",
                hid: "description"
            },
			{
				name: "theme-color",
                content: "#ff9900",
                hid: "theme-color"
			},
            {
				property: "og:title",
                content: "Blogger",
                hid: "og:title"
            },
            {
				property: "og:site_name",
                content: "Blogger",
                hid: "og:site_name"
            },
            {
				property: "og:description",
                content: "Place to blog",
                hid: "og:description"
            },
        ],
        link: [{
                rel: "icon",
                type: "image/png",
                href: "/img/icon-192.png"
            },
        ]
    },
    //Progress-bar color
    loading: {
        color: "#FF9900"
    },

    serverMiddleware: [
        cookieParser(),
        decodeJWT
    ],
    // Plugins
    plugins: [
        "~/plugins/vuelidate",
    ],

    buildModules: [
        '@nuxtjs/vuetify',
        '@nuxtjs/svg',
        '@nuxtjs/style-resources',
    ],

    //Nuxt.js modules
    modules: [
        '@nuxtjs/axios',
    ],
    vuetify: {
        defaultAssets: { font: { family: "Roboto" }, icons: "mdi" },
        // Use to override vuetify defaults. Works only with tree-shaking (production mode by default).
        // Using tree-shaking in development mode will increase build time considerably.
        // customVariables: [ "~/assets/variables.scss" ],
        // treeShake: true,
        theme: {
            options: {
                customProperties: true,
            },
            themes: {
                light: {
                    primary:                "#ff9900",  //teal
                    secondary:              "#00BCD4",  //cyan
                    accent:                 "#FF9900",  //indigo
                    background:             "#f5f5f5",  //grey lighten-4
                    "background-secondary": "#eeeeee"   //grey lighten-3
                },
                dark: {
                    primary:                "#009688",  //teal
                    secondary:              "#41b883",  //green darken-2
                    accent:                 "#EC407A",  //pink lighten-1
                    background:             "#282828",  //grey darken-3
                    "background-secondary": "#282828"   //custom
                },
                support:                "#00897B",  //teal darken-1
                error:                  "#f44336",  //red
                warning:                "#ff5722",  //deep-orange
                info:                   "#00bcd4",  //cyan
                success:                "#4caf50",  //green
            }
        }
    },
    axios: {
        proxy: true
    },
    proxy: {
        "/api/": { target: backEndUrl, xfwd: true, toProxy: true, pathRewrite: { "^/api/": "/" }, onProxyReq: ( proxyReq, req, res ) => {
            if( req.body ){
                const bodyData = JSON.stringify( req.body );
                proxyReq.setHeader( "Content-Type","application/json" );
                proxyReq.setHeader( "Content-Length", Buffer.byteLength( bodyData ));
                proxyReq.write( bodyData );
            }
        }}
    },
}
