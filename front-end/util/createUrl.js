module.exports = ( req, url ) => {
  if( process.server )
    return "http://blogger-api:9000" + url.replace( "/api", "" );
  else
    return url;
};