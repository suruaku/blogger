import axios from "axios";

export const state = () => ({
  user: null,
  token: null
});
  
export const mutations = {
  SET_USER: ( state, user ) => {
    	state.user = user;
  },
  SET_TOKEN: ( state, token ) => {
    	state.token = token;
  	}
};

export const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  async nuxtServerInit({ commit, dispatch }, { req }){
    if( req.cookies ){
      if( req.cookies.authorization )
        commit( "SET_TOKEN", req.cookies.authorization );
    }
    if( req.token ){
      if( req.token.user ){
        commit( "SET_USER", req.token.user );
      }
    }
  },
  clientInit({ state, dispatch }, context ){
    const min5 = 1000 * 60 * 5;

    setInterval(() => {
      if( state.user ){
        dispatch( "refreshUser", context );
      }
    }, min5 );
  },
  async login({ commit }, { username, password }){
        
    try {
      const { data } = await axios.post( "/api/auth/login", { username, password });

      commit( "SET_USER", data.user );
    } catch( err ){
      throw err;
    }
  },
  async logout({ commit }){
    await axios.post( "/api/auth/logout" );
    commit( "SET_USER", null );
    commit( "SET_TOKEN", null );
  },
  async refreshUser({ state, commit }, context ){

    if( !state.user )
      return;

    try {
      const { data } = await axios.get( `/api/auth/refresh/${ state.user._id }` );

      commit( "SET_USER", data.user );
      commit( "SET_TOKEN", data.token );
    } catch( err ){}
  },
};
