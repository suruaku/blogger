export default async context => {
  if( process.client ){
    await context.store.dispatch( "clientInit", context );
  }
};