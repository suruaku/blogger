const { promisify }     = require( "util" );

const jwt               = require( "jsonwebtoken" );


const verify            = promisify( jwt.verify );
const jwtSecret         = process.env.JWT_SECRET;


module.exports = async ( req, res, next ) => {
  const token = req.cookies.authorization;
  if( !token ){
    return next();
  }
  let decodedToken;
  try {
    decodedToken = await verify( token, jwtSecret );
  } catch( err ){}
  if( !decodedToken ){
    return next();
  }
  req.token = decodedToken;
  return next();
};
