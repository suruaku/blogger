export const requestErrors = {
  NETWORK_ERROR_ORIGINAL: "A network error (such as timeout, interrupted connection or unreachable host) has occurred",
  NETWORK_ERROR_TITLE: "Something went wrong",
  NETWORK_ERROR_TEXT: "The server might be updating or offline temporarily, please try again soon",
  UNKNOWN: "Something went wrong"
};

export const formErrors = {
  FORM_ERROR_TITLE: "Form not filled correctly",
  FORM_ERROR_TEXT: "Please fill the parts marked in red correctly"
};