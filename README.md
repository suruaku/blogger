## How to use this project
To run the app locally you need Docker, docker-compose and make(optional).
Front-end runs at port 3000 and API at port 9000.

Run in development:
```
make dev
```
Run in production:
```
make up
```
Remember to create `production.env` for production envinronment.

Build new image and push it to image registry(i.e. gitlab). The image will be tagged with short SHA of current git commit. For example `front-57c3943` and `api-57c3943`. For this you need docker-slim tool.

API image:
```
./update.sh api
```
Front-end image:
```
./update.sh front
```

Pull new images and update deployment(production):
```
make update
```


## API docs
### Post routes
Post object form:
```
    {
        "body": "This is example post.",
        "author": "ilja"
    }
```


| Method | URI | Description |
| ------ | ------ | ------ |
| GET | /api/posts | Get all posts |
| GET | /api/posts/:user | Get posts by username |
| POST | /api/post | Add post |
| DELETE | /api/post | Delete post |

### User routes
User object form:
```
    {
        "username": "ilja",
        "password": "pswd1234" 
    }
```
| Method | URI | Description |
| ------ | ------ | ------ |
| GET | /auth/refresh/:userId | Refresh JWT token |
| POST | /auth/user | Register user |
| POST | /auth/login | Log in user |
| POST | /auth/logout | Log out user |
