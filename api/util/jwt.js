const { promisify } = require( "util" );

const jwt           = require( "jsonwebtoken" );

const sign          = promisify( jwt.sign );
const verify        = promisify( jwt.verify );


exports.signJWT = async ( req, res, payload ) => {
  const oldToken = { ...req.token };
  delete oldToken.iat;
  delete oldToken.exp;
  const token = await sign({
    ...oldToken,
    ...payload
  }, process.env.JWT_SECRET, { expiresIn: "24h" });
  res.setCookie( "authorization", token, { 
    path: "/",
    maxAge: 1000 * 60 * 60 * 24, 
    httpOnly: true, 
    sameSite: true,
  });
  return token;
};

exports.decodeJWT = async ( req, res ) => {
  const token = req.cookies.authorization;
  if( !token ){
    return;
  }
  let decodedToken;
  try {
    decodedToken = await verify( token, process.env.JWT_SECRET );
  } catch( err ){}
  if( !decodedToken ){
    return;
  }
  req.token = decodedToken;
  return;
};