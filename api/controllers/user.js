const { join }       = require( "path" );

const boom           = require( "@hapi/boom" );
const mongoose       = require( "mongoose" );
const bcrypt         = require( "bcryptjs" );

const { signJWT }    = require( join( __rootPath, "util", "jwt" ));

const User           = require( join( __rootPath, "models", "user" ));

/****************************** Users ******************************/
// /api/auth/refresh/:userId => GET
exports.getRefreshUser = async ( req, res ) => {
  const userId = req.params.userId;

  if( userId !== req.token.user._id.toString() ){
    return res.send( 403 ).send({ message: "You can only access your profile" });
  }

  try { 
    const user = await User.findById( userId ).lean();

    if( !user ){
      return res.code( 404 ).send({ message: "User not found" });
    }

    delete user.password;

    const payload = { user: { ...user, _id: user._id.toString() }};
    const token = await signJWT( req, res, payload );

    res.code( 200 ).send({ user, token });
  } catch ( err ){
    throw boom.boomify( err );
  }
};

// /api/auth/user => POST
exports.postAddUser = async ( req, res ) => {
  const user = req.body.user;
    
  try {
    const oldUser = await User.findOne({ username: user.username }).lean();

    if( oldUser ){
      return res.code( 422 ).send({ type: "error", message: "User with this username already exists" });
    }

    user.password = await bcrypt.hash( user.password, parseInt( process.env.PASSWORD_HASH_ROUNDS ));

    let newUser;
    let token;

    const session = await mongoose.startSession();
    await session.withTransaction( async () => {
      [ newUser ] = await User.create([ user ], { session });

      delete newUser._doc.password;

      const payload = { user: { ...newUser._doc, _id: newUser._id.toString() }};
      token = await signJWT( req, res, payload );
    });

    res.code( 201 ).send({ user: newUser, token });
  } catch ( err ){
    throw boom.boomify( err );
  }
};

// /api/auth/login => POST
exports.postLogin = async ( req, res ) => {
  const username     = req.body.username;
  const password  = req.body.password;

  try {
    const user = await User.findOne({ username }).select( "name username password" ).lean();

    if( !user ){
      return res.code( 401 ).send({ message: "Incorrect username or password" });
    }

    const match = await bcrypt.compare( password, user.password );

    if( !match ){
      return res.code( 401 ).send({ message: "Incorrect username or password" });
    }

    delete user.password;

    const payload = { user: { ...user, _id: user._id.toString() }};
    const token = await signJWT( req, res, payload );

    res.code( 200 ).send({ user, token });
  } catch( err ){
    throw boom.boomify( err );
  }
};

// /api/auth/logout => POST
exports.postLogout = async ( req, res ) => {
  res.code( 204 ).clearCookie( "authorization" );
};
/***************************** !Users ******************************/