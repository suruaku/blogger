const { join }       = require( "path" );

const boom           = require( "@hapi/boom" );

const Post           = require( join( __rootPath, "models", "post" ));

/****************************** Posts ******************************/
// /api/posts => GET
exports.getAllPosts = async ( req, res ) => {
  try {
    const posts = await Post.find().lean();

    res.code( 200 ).send({ posts });
  } catch( err ){
    res.code( 404 );
    throw boom.boomify( err );
  }
};

// /api/posts/:user => GET
exports.getUserPosts = async ( req, res ) => {
  const author = req.params.user;
    
  try {
    const posts = await Post.find({ author: author }).lean();

    if( !posts ){
      return res.code( 404 ).send({ message: "Posts not found" });
    }

    res.code( 200 ).send({ posts });
  } catch( err ){
    res.code( 404 );
    throw boom.boomify( err );
  }
};

// /api/post => POST
exports.postAddPost = async ( req, res ) => {
  const newPost = req.body;

  try{
    if( newPost.author.toString() !== req.token.user.username.toString() )
      res.code( 403 ).send({ message: "You can create post from your profile only"});

    const post = await new Post( newPost ).save();

    res.code( 201 ).send({ post });
  } catch( err ){
    res.code( 404 );
    throw boom.boomify( err );
  }
};


// /api/post => DELETE
exports.deletePost = async ( req, res ) => {
  const id = req.query.id;

  try{ 
    const postToDelete = await Post.findById( id ).lean();

    if( postToDelete.author.toString() !== req.token.user.username.toString() )
      res.code( 403 ).send({ message: "You can delete post from your profile only"});

    await Post.findByIdAndRemove( id );

    res.code( 204 );
  } catch( err ){
    res.code( 404 );
    throw boom.boomify( err );
  }
};
/***************************** !Posts ******************************/