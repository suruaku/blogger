const mongoose = require( "mongoose" );

const postSchema = new mongoose.Schema({
  body: {
    type: String,
    required: true,
    maxLength: 200
  },
  author: {
    type: String,
    required: true
  },
}, { timestamps: true });


const model =  mongoose.model( "Post", postSchema );

setTimeout(() => model.createCollection(), 2000 );

module.exports = model; 