const mongoose = require( "mongoose" );

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
}, { timestamps: true, autoIndex: false });


const model =  mongoose.model( "User", userSchema );

setTimeout(() => model.createCollection(), 2000 );

module.exports = model;