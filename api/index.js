// Node core pkgs
const { join }      = require( "path" );

// Globals
global.__rootPath   = __dirname;

// 3rd party pkgs
const fastify       = require( "fastify" )({ logger: true, trustProxy: true });
const mongoose      = require( "mongoose" );
const helmet        = require( "helmet" );

// Own pkgs
const routes        = require( join( __dirname, "routes" ));
const swagger       = require( join( __rootPath, "config", "swagger" ) );
const { decodeJWT } = require( join( __rootPath, "util", "jwt" ));

// Fastify plugins
fastify.register( require( "fastify-cookie" ));
fastify.register( require( "fastify-swagger" ), swagger.options );

fastify.use( helmet() );
fastify.use(( req, res, next ) => {
  res.setHeader( "Access-Control-Allow-Origin", "*" );                                //Allows any domain to access data (CORS).
  res.setHeader( "Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE" );   //Allows these methods for allowed origins.
  res.setHeader( "Access-Control-Allow-Headers", "Content-Type, Authorization" );
  next();
});

fastify.decorate( "mongoose", mongoose );

// Decode JWT
fastify.addHook("preValidation", async ( req, res ) => {
  await decodeJWT( req, res );
  return;
});

// Declare routes
routes.forEach(( route, index )=> {
  fastify.route( route );
});

// Connect to DB and start the server
const start = async () => {
  try{
    await mongoose.connect( process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
    await fastify.listen( process.env.PORT, process.env.HOST );

    fastify.swagger();
    fastify.log.info( `Listening on ${ fastify.server.address().port }`);
  } catch ( err ) {
    fastify.log.error( err );
    process.exit( -1 );
  }
};

start();

module.exports = fastify;
