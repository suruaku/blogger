exports.options = {
  routePrefix: "/documentation",
  exposeRoute: true,
  swagger: {
    info: {
      title: "Blogger API",
      description: "This is API documentation for blogger.kotir.space. It is built using Node.js, Fastify and MongoDB.",
      version: "0.0.1"
    },
    externalDocs: {
      url: "https://blogger.kotir.space",
      description: "Find more info here"
    },
    host: "blogger.kotir.space",
    schemes: ["http"],
    consumes: ["application/json"],
    produces: ["application/json"],
    tags: [
      { name: "user", description: "User related end-points" },
      { name: "code", description: "Code related end-points" }
    ],
    definitions: {
      User: {
        $id: "User",
        type: "object",
        required: ["id", "email"],
        properties: {
          id: { type: "string", format: "uuid" },
          firstName: { type: "string", nullable: true },
          lastName: { type: "string", nullable: true },
          email: {type: "string", format: "email" }
        }
      }
    },
  }
};