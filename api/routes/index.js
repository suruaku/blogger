const path              = require( "path" );

// Controllers
const userController    = require( path.join( __rootPath, "controllers", "user" ));
const postController    = require( path.join( __rootPath, "controllers", "post" ));

const routes = [
  /******************! USER !********************/
  {
    method: "GET",
    url: "/auth/refresh/:userId",
    handler: userController.getRefreshUser,
  },
  {
    method: "POST",
    url: "/auth/user",
    handler: userController.postAddUser,
  },
  {
    method: "POST",
    url: "/auth/login",
    handler: userController.postLogin,
  },
  {
    method: "POST",
    url: "/auth/logout",
    handler: userController.postLogout,
  },
  /******************* USER *********************/
  /******************! Blog-posts !********************/
  {
    method: "GET",
    url: "/posts",
    handler: postController.getAllPosts,
  },
  {
    method: "GET",
    url: "/posts/:user",
    handler: postController.getUserPosts,
  },
  {
    method: "POST",
    url: "/post",
    handler: postController.postAddPost,
  },
  {
    method: "DELETE",
    url: "/post",
    handler: postController.deletePost,
  },
  /******************* Blog-posts *********************/
];

module.exports = routes;