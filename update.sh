#!/bin/bash
# Build, tag(with short git sha) and push image to gitlab registry

if [ "$1" == "front" ]; then
    cd "$PWD"/front-end || exit

    TAG=$(git rev-parse --short HEAD)
    IMAGE_FRONT="registry.gitlab.com/lut1/blogger:front-$TAG"

    docker-slim build --dockerfile Dockerfile --tag "$IMAGE_FRONT" --include-path=/var/www/front-end --include-path=/usr/local/lib/node_modules/npm/node_modules/node-gyp . && docker push "$IMAGE_FRONT"
    notify-send "Image updated and tagged front-$TAG"
fi

if [ "$1" == "api" ]; then
    cd "$PWD"/api || exit

    TAG=$(git rev-parse --short HEAD)
    IMAGE_API="registry.gitlab.com/lut1/blogger:api-$TAG"

    docker build -t "$IMAGE_API" . && docker push "$IMAGE_API"
    notify-send "Image updated and tagged api-$TAG"
fi

